﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace bgj7
{
    class Platform
    {
        public Sprite sprite;

        public Rectangle spawnArea
        {
            get { return new Rectangle((int)sprite.position.X - 20, (int)sprite.position.Y - 20, sprite.drawRectangle.Width + 40, sprite.drawRectangle.Height + 40); }
        }

        public Rectangle topBox
        {
            get { return new Rectangle(sprite.drawRectangle.X, sprite.drawRectangle.Y, sprite.drawRectangle.Width, 1); }
        }

        public Platform(Sprite sprite, float size) 
        {
            this.sprite = sprite;

            sprite.drawRectangle.Width =  (int)(sprite.drawRectangle.Width * size);
        }

        public void Update(GameTime gameTime)
        {

        }

        public void Move(int X, int Y)
        {
            sprite.drawRectangle.X += X;
            sprite.drawRectangle.Y += Y;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            sprite.Draw(spriteBatch);
        }
    }
}
