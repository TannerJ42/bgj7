﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace bgj7
{
    class Sprite
    {
        public bool Active = true;
        public bool Visible = true;

        public Vector2 position;
        public Rectangle drawRectangle;
        public Rectangle sourceRectangle;

        public Texture2D image;

        SpriteEffects facing = SpriteEffects.None;

        public bool facingLeft
        {
            get { return facing == SpriteEffects.FlipHorizontally; }
        }

        public Sprite(Texture2D sprite, Vector2 location)
        {
            drawRectangle = new Rectangle((int)location.X, (int)location.Y, sprite.Width, sprite.Height);
            sourceRectangle = new Rectangle(0, 0, sprite.Width, sprite.Height);
            image = sprite;
            position = location;
        }

        public void FaceLeft()
        {
            facing = SpriteEffects.FlipHorizontally;
        }

        public void FaceRight()
        {
            facing = SpriteEffects.None;
        }

        public virtual void Update(GameTime gameTime)
        {

        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            drawRectangle.X = (int)position.X;
            drawRectangle.Y = (int)position.Y;
            spriteBatch.Draw(image, drawRectangle, sourceRectangle, Color.White, 0f, new Vector2(), facing, 0);
        }
    }
}
