﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace bgj7
{
    class landLayer : Layer
    {
        int top = 400;

        Texture2D background;
        Vector2 position1;
        Vector2 position2;

        public landLayer(GameManager game)
            : base(game, 1f)
        {
            background = game.GetTexture2D("background-land");
            position1 = new Vector2(0, 220);
            position2 = new Vector2(0, 220);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (position1.X + background.Width < 0)
            {
                position1.X += background.Width;
            }
        }

        public override void Move(float distance)
        {
            base.Move(distance);
            position1.X -= distance * multiplier;
            position2.X = position1.X + background.Width;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            spriteBatch.Draw(background, position1, Color.White);
            spriteBatch.Draw(background, position2, Color.White);

        }
    }
}
