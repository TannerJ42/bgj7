﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace bgj7
{
    class mountainLayer : Layer
    {
        int top = 0;

        Texture2D background;
        Vector2 position1;
        Vector2 position2;

        public mountainLayer(GameManager game)
            : base(game, 0.1f)
        {
            background = game.GetTexture2D("background_mt");
            position1 = new Vector2(0, top);
            position2 = new Vector2(0, top);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (position1.X + background.Width < 0)
            {
                position1.X += background.Width;
            }
        }

        public override void Move(float distance)
        {
            base.Move(distance);
            position1.X -= distance * multiplier;
            position2.X = position1.X + background.Width;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            spriteBatch.Draw(background, position1, Color.White);
            spriteBatch.Draw(background, position2, Color.White);
            //if (sourceRectangle.Right > background.Width)
            //{
            //    spriteBatch.Draw(background, new Vector2(drawRectangle.Right, drawRectangle.Y), Color.White);
            //}
        }
    }
}
