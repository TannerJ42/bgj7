﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace bgj7
{
    class Layer
    {
        GameManager game;
        protected List<Sprite> elements;

        protected float multiplier;

        public Layer(GameManager game, float multiplier)
        {
            this.game = game;
            elements = new List<Sprite>();
            this.multiplier = multiplier;
        }

        public virtual void Move(float distance)
        {
            foreach (Sprite s in elements)
            {
                s.position.X -= (multiplier * distance);
            }
        }

        public virtual void Update(GameTime gameTime)
        {

        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            foreach (Sprite s in elements)
            {
                s.Draw(spriteBatch);
            }
        }
    }
}
