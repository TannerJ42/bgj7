﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace bgj7
{
    class Scriptor
    {
        GameManager game;
        List<Platform> platforms;
        List<Projectile> projectiles;
        List<NPC> npcs;
        List<Level> levels;
        public int currentLevel = -1;

        int remainingNPCs;

        float timeUntilPlatformSpawn = 0;

        float timeUntilProjectileSpawn = 5;

        float distance;

        public Scriptor(GameManager game)
        {
            this.game = game;

            levels = new List<Level>();

            for (int i = 0; i < Levels.distances.Length; i++ )
            {
                levels.Add(new Level(Levels.distances[i], Levels.platformMinY[i], Levels.platformMaxY[i], Levels.platformSpawnPeriod[i], Levels.chanceForAnotherPlatform[i], 
                    Levels.projectileMinY[i], Levels.projectileMaxY[i], Levels.projectileSpawnPeriod[i], Levels.chanceForAnotherProjectile[i],
                    Levels.chanceForNPC[i], Levels.maxNPCs[i]));
            }

            ChangeLevel();
        }

        public void AssignPlatforms(ref List<Platform> platforms)
        {
            this.platforms = platforms;
        }

        public void AssignProjectiles(ref List<Projectile> projectiles)
        {
            this.projectiles = projectiles;
        }

        public void AssignNPCs(ref List<NPC> npcs)
        {
            this.npcs = npcs;
        }

        public void Move(float distance)
        {
            timeUntilPlatformSpawn -= distance;

            if (timeUntilPlatformSpawn <= 0)
            {
                SpawnPlatform();
                timeUntilPlatformSpawn = levels[currentLevel].platformSpawnPeriod;
            }

            this.distance += distance;
        }

        public void Update(GameTime gameTime)
        {
            timeUntilProjectileSpawn -= (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (timeUntilProjectileSpawn <= 0)
            {
                SpawnProjectile();
                timeUntilProjectileSpawn = levels[currentLevel].projectileSpawnPeriod;
            }

            if (distance > levels[currentLevel].distance)
            {
                distance = 0;
                ChangeLevel();
            }
        }

        public void ChangeLevel()
        {
            currentLevel += 1;

            //if (currentLevel == 8)
            //    game.storyTeller.level10 = true;

            if (currentLevel > levels.Count - 1)
                currentLevel = levels.Count - 1;
            else if (currentLevel == levels.Count - 1)
            {
                game.storyTeller.gameOver = true;
                GameWon = true;
            }
            remainingNPCs = levels[currentLevel].maxNPCs;
            timeUntilProjectileSpawn = levels[currentLevel].projectileSpawnPeriod;
            timeUntilPlatformSpawn = levels[currentLevel].platformSpawnPeriod;

            Console.WriteLine("Reached level " + currentLevel);
        }

        public void SpawnProjectile()
        {

            Projectile projectile = new Projectile(game, 0);

            bool done = false;

            int counter = 0;

            while (!done)
            {
                ChooseProjectileSpawnArea(projectile);
                done = true;
                foreach (Projectile p in projectiles)
                {
                    if (p.drawRectangle.Intersects(projectile.drawRectangle))
                    {
                        done = false;
                    }
                }

                if (counter++ > 25)
                    done = true;
            }

            projectile.drawRectangle.Y = (int)projectile.position.Y;

            projectiles.Add(projectile);

            if (game.rand.Next(100) <= levels[currentLevel].chanceForAnotherProjectile)
                SpawnProjectile();
        }

        public void SpawnPlatform()
        {
            float size = (game.rand.Next(80, 121)) / 100f;

            Platform platform = new Platform(new Sprite(game.GetTexture2D("platform"), new Vector2()), size);

            bool done = false;

            int counter = 0;

            while (!done)
            {
                ChooseSpawnArea(platform);
                done = true;
                foreach (Platform p in platforms)
                {
                    if (p.spawnArea.Intersects(platform.spawnArea))
                    {
                        done = false;
                    }
                }

                if (counter++ > 50)
                    done = true;
            }

            platforms.Add(platform);

            int random = game.rand.Next(100);

            if (random <= levels[currentLevel].chanceForNPC &&
                remainingNPCs > 0)
            {
                remainingNPCs -= 1;
                npcs.Add(new NPC(game, platform));
            }
            if (game.rand.Next(100) <= levels[currentLevel].chanceForAnotherPlatform)
                SpawnPlatform();
        }

        void ChooseSpawnArea(Platform platform)
        {
            platform.sprite.position.X = game.rand.Next(1280, 2000);
            platform.sprite.position.Y = game.rand.Next(levels[currentLevel].platformYMin, levels[currentLevel].platformYMax - 30);
        }

        void ChooseProjectileSpawnArea(Projectile projectile)
        {
            projectile.position.X = game.rand.Next(1280, 2000);
            projectile.position.Y = game.rand.Next(levels[currentLevel].projectileYMin, levels[currentLevel].projectileYMax - 30);
        }

        public bool GameWon = false;
    }
}
