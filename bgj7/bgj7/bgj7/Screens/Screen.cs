﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace bgj7
{
    class Screen
    {
        bool loaded = false;
        bool initialized = false;
        public GameManager game;

        public Player player;

        /// <summary>
        /// Is content ready to display.
        /// </summary>
        public bool Loaded
        {
            get { return loaded; }
        }

        /// <summary>
        /// Is screen ready to run.
        /// </summary>
        public bool Initialized
        {
            get { return initialized; }
        }

        public Screen(GameManager game, Player player)
        {
            this.game = game;
            this.player = player;
        }

        /// <summary>
        /// Call when you're ready to set up the screen, level, etc.
        /// </summary>
        public virtual void Initialize()
        {
            initialized = true;
        }

        /// <summary>
        /// Reset values
        /// </summary>
        public virtual void Uninitialize()
        {
            initialized = false;
        }

        /// <summary>
        /// Screen becomes main screen.
        /// </summary>
        public virtual void Enter()
        {
            
        }

        /// <summary>
        /// Screen goes into background.
        /// </summary>
        public virtual void Leave()
        {

        }

        /// <summary>
        /// Load assets
        /// </summary>
        public virtual void Load()
        {
            loaded = true;
        }

        /// <summary>
        /// Unload assets
        /// </summary>
        public virtual void Unload()
        {
            loaded = false;
        }

        /// <summary>
        /// Do stuff.
        /// </summary>
        /// <param name="gameTime"></param>
        public virtual void Update(GameTime gameTime)
        {

        }

        /// <summary>
        /// Make stuff appear.
        /// </summary>
        /// <param name="spriteBatch"></param>
        public virtual void Draw(SpriteBatch spriteBatch)
        {

        }
    }
}
