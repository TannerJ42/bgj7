﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace bgj7
{
    class gameOverScreen : Screen
    {
        SpriteFont font;
        Sprite background;
        Vector2 location = new Vector2(350, 200);

        public gameOverScreen(GameManager game, Player player)
            : base(game, player)
        {

        }

        public override void Enter()
        {
            base.Enter();

            
        }

        public override void Load()
        {
            background = new Sprite(game.GetTexture2D("GameOver"), new Vector2());
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (player.PressedEnter())
            {
                game.ChangeScreen("intro");
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            game.ClearScreen(Color.Black);
            background.Draw(spriteBatch);
            //spriteBatch.DrawString(font, "Game Over", location, Color.DarkRed); 
        }
    }
}
