﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace bgj7
{
    class creditsScreen : Screen
    {

        public creditsScreen(GameManager game, Player player)
            : base(game, player)
        {

        }

        public override void Load()
        {
            //introMusic = game.audioManager.GetCue("intro");
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (player.PressedEnter())
            {
                game.ChangeScreen("intro");
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            game.ClearScreen(Color.Pink);
        }
    }
}
