﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace bgj7
{
    class introScreen : Screen
    {
        Sprite background;
        Cue introMusic;

        public introScreen(GameManager game, Player player)
            : base(game, player)
        {
            
        }

        public override void Enter()
        {
            base.Enter();

            introMusic.Play();

        }

        public override void Leave()
        {
            base.Leave();

            introMusic.Stop(AudioStopOptions.AsAuthored);
        }

        public override void Load()
        {
            introMusic = game.audioManager.GetCue("BGM_Int1");
            background = new Sprite(game.GetTexture2D("TitleScreen_Instruction"), new Vector2(0, 0));
            
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (player.PressedEnter())
            {
                game.ChangeScreen("test");
            }

            if (!introMusic.IsPlaying)
            {
                introMusic = game.audioManager.GetCue("BGM_Int1");
                introMusic.Play();
            }

        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            game.ClearScreen(Color.White);
            background.Draw(spriteBatch);
        }
    }
}
