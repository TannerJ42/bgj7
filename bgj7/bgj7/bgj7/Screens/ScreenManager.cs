﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace bgj7
{
    class ScreenManager
    {
        Dictionary<string, Screen> screens;
        Screen currentScreen;

        GameManager game;

        public ScreenManager(GameManager game)
        {
            this.game = game;
            screens = new Dictionary<string, Screen>();
            screens.Add("intro", new introScreen(game, game.Player));
            screens.Add("test", new testScreen(game, game.Player));
            screens.Add("gameOver", new gameOverScreen(game, game.Player));
            screens.Add("credits", new creditsScreen(game, game.Player));

            //currentScreen = screens["intro"];
            ChangeScreen("intro");
        }

        ///// <summary>
        ///// Adds a screen to the dict
        ///// </summary>
        ///// <param name="id"></param>
        ///// <param name="screen"></param>
        //public void AddScreen(string id, Screen screen)
        //{
        //    screens.Add(id, screen);
        //}

        /// <summary>
        /// Make screen current.
        /// </summary>
        /// <param name="screen"></param>
        public void ChangeScreen(string name)
        {
            Screen screen = screens[name];

            if (!(currentScreen == null))
                currentScreen.Leave();

            if (!screen.Loaded)
                screen.Load();
            if (!screen.Initialized)
                screen.Initialize();
            currentScreen = screen;
            screen.Enter();
        }

        /// <summary>
        /// Do stuff.
        /// </summary>
        /// <param name="gameTime"></param>
        public virtual void Update(GameTime gameTime)
        {
            currentScreen.Update(gameTime);
        }

        /// <summary>
        /// Make stuff appear.
        /// </summary>
        /// <param name="spriteBatch"></param>
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            currentScreen.Draw(spriteBatch);
        }
    }
}
