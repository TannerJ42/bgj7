﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace bgj7
{
    class testScreen : Screen
    {
        Gatherer gatherer;
        List<Platform> platforms;

        Sprite landmark1;
        Sprite landmark2;

        StoryTeller storyTeller;

        grassLayer grass;
        mountainLayer mountains;
        starLayer stars;
        landLayer land;

        Death death;

        List<Projectile> projectiles;
        List<NPC> npcs;

        Scriptor scriptor;

        Cue bgm;

        bool introPlayed = false;

        GameplayState gameplayState;

        float introTimer;

        Sprite graveFront;
        Sprite graveBack;

        bool shownLandmark1 = false;
        bool shownLandmark2 = false;

        enum GameplayState
        {
            start,
            gameplay,
            end
        }

        public testScreen(GameManager game, Player player)
            : base(game, player)
        {

        }

        public override void Load()
        {

        }

        public override void Enter()
        {
            base.Enter();
            gatherer = new Gatherer(player, game, this);
            platforms = new List<Platform>();
            npcs = new List<NPC>();

            scriptor = new Scriptor(game);

            grass = new grassLayer(game);
            mountains = new mountainLayer(game);
            stars = new starLayer(game);
            land = new landLayer(game);

            death = new Death(game);

            projectiles = new List<Projectile>();

            gatherer.SetReferences(death, ref projectiles);
            scriptor.AssignPlatforms(ref platforms);
            scriptor.AssignProjectiles(ref projectiles);
            scriptor.AssignNPCs(ref npcs);

            introTimer = 0;

            introPlayed = false;

            //bgm = game.audioManager.GetCue("BGM_Start");

            storyTeller = new StoryTeller(game, player, gatherer, death, ref platforms, ref projectiles);

            game.AssignStoryTeller(storyTeller);

            gameplayState = GameplayState.start;

            graveBack = new Sprite(game.GetTexture2D("graveyardBack"), new Vector2());
            graveFront = new Sprite(game.GetTexture2D("graveyardFront"), new Vector2());

            graveBack.FaceLeft();
            graveFront.FaceLeft();

            //bgm.Play();

            game.storyTeller.TellStory("dx_DeathStart01");
            //game.storyTeller.TellStory("BGM_Intro");

            bgm = game.audioManager.GetCue("Thirst_BGM_Intro");
            bgm.Play();

            landmark1 = new Sprite(game.GetTexture2D("level8"), new Vector2(-3000, 50));
            landmark2 = new Sprite(game.GetTexture2D("level9"), new Vector2(-3000, 50));
        }

        public override void Leave()
        {
            base.Leave();

            bgm.Stop(AudioStopOptions.Immediate);

            Console.WriteLine("Leaving!");
        }

        public void Scroll(float amount)
        {
            for (int i = platforms.Count - 1; i >= 0; i--)
            {
                platforms[i].sprite.position.X -= amount;
                if (platforms[i].sprite.drawRectangle.Right < 0)
                {
                    platforms.RemoveAt(i);
                }
            }

            for (int i = projectiles.Count - 1; i >= 0; i--)
            {
                projectiles[i].position.X -= amount;
                if (projectiles[i].drawRectangle.Right < 0)
                {
                    projectiles.RemoveAt(i);
                }
            }

            for (int i = npcs.Count - 1; i >= 0; i--)
            {
                npcs[i].position.X -= amount;
                npcs[i].deadSprite.position.X -= amount;
                if (npcs[i].drawRectangle.Right < 0)
                {
                    npcs.RemoveAt(i);
                }
            }

            death.Move(amount);
            graveBack.position.X -= amount * 0.8f;
            graveFront.position.X -= amount;
            landmark1.position.X -= amount * 0.8f;
            landmark2.position.X -= amount * 0.8f;
            stars.Move(amount);
            land.Move(amount);
            grass.Move(amount);
            mountains.Move(amount);

            scriptor.Move(amount);
        }

        public override void Update(GameTime gameTime)
        {

            if (!introPlayed &&
                bgm.IsStopped)
            {
                introPlayed = true;
                game.storyTeller.PlayBGM("BGM_Int1");
            }

            if (gameplayState == GameplayState.start)
            {
                storyTeller.Update(gameTime);
                gatherer.Update(gameTime, platforms, npcs, death);
                if (!gatherer.moveForward)
                {
                    gatherer.moveForward = true;
                    gatherer.moveForwardSpeed = 10;
                }

                gatherer.moveForwardSpeed += 0.1f;

                if (introTimer < 15)
                {
                    introTimer += (float)gameTime.ElapsedGameTime.TotalSeconds;
                }
                else
                {
                    gatherer.CanMove = true;
                }
                if (introPlayed == true)
                {
                    gatherer.CanMove = true;
                    gameplayState = GameplayState.gameplay;
                    gatherer.moveForward = false;
                }
            }

            if (gameplayState == GameplayState.gameplay)
            {

                base.Update(gameTime);


                storyTeller.Update(gameTime);
                gatherer.Update(gameTime, platforms, npcs, death);
                

                grass.Update(gameTime);
                mountains.Update(gameTime);
                stars.Update(gameTime);
                land.Update(gameTime);
                landmark1.Update(gameTime);
                landmark2.Update(gameTime);

                foreach (Projectile p in projectiles)
                    p.Update(gameTime, gatherer);

                foreach (NPC npc in npcs)
                    npc.Update(gameTime);

                death.Update(gameTime);

                scriptor.Update(gameTime);

                if (death.GameLost)
                {
                    game.audioManager.PlayCue("vx_NitocrisDeath");
                    game.audioManager.PlayCue("dx_DeathWin01");
                    game.ChangeScreen("gameOver");
                    storyTeller.StopAudio();
                    //game.audioManager.PlayCue("dx_DeathLose01");
                }

                if (scriptor.GameWon)
                {
                    gatherer.CanMove = false;
                    gatherer.moveForward = true;
                    gatherer.moveForwardSpeed = CONSTANTS.speed_limit;
                    gameplayState = GameplayState.end;
                    game.audioManager.PlayCue("dx_DeathLose01");
                    Console.WriteLine("You won!");
                    graveFront.image = game.GetTexture2D("graveyardFrontWin");
                    graveBack.image = game.GetTexture2D("graveyardBackWin");

                    graveBack.position.X = 1280;
                    graveFront.position.X = 1280;
                    graveBack.FaceRight();
                    graveFront.FaceRight();

                    for (int i = platforms.Count - 1; i >= 0; i--)
                    {
                        if (platforms[i].sprite.position.X > 1280)
                            platforms.RemoveAt(i);
                    }
                    for (int i = projectiles.Count - 1; i >= 0; i--)
                    {
                        if (projectiles[i].position.X > 1280)
                            projectiles.RemoveAt(i);
                    } 
                    for (int i = npcs.Count - 1; i >= 0; i--)
                    {
                        if (npcs[i].position.X > 1280)
                            npcs.RemoveAt(i);
                    }
                }
            }

            if (gameplayState == GameplayState.end)
            {
                //if (gatherer.moveForwardSpeed > 10)
                //    gatherer.moveForwardSpeed -= 0.1f;
                gatherer.Move(0.5f, 0);
                storyTeller.Update(gameTime);
                gatherer.Update(gameTime, platforms, npcs, death);
                death.Move(20);

                if (!gatherer.gameOver &&
                    graveFront.position.X <= 0)
                {
                    gatherer.gameOver = true;
                    graveFront.position.X = 0;
                }

                if (storyTeller.returnToTitle)
                {
                    game.ChangeScreen("intro");
                }

                gatherer.Ground();
            }

            if (scriptor.currentLevel == 8 &&
                !shownLandmark1)
            {
                shownLandmark1 = true;
                landmark1.position.X = 1280;
            }
            if (scriptor.currentLevel == 9 &&
                !shownLandmark2)
            {
                shownLandmark2 = true;
                landmark2.position.X = 1280;
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            game.ClearScreen(Color.HotPink);
            stars.Draw(spriteBatch);
            mountains.Draw(spriteBatch);
            grass.Draw(spriteBatch);

            landmark1.Draw(spriteBatch);
            landmark2.Draw(spriteBatch);

            graveBack.Draw(spriteBatch);

            foreach (Platform p in platforms)
                p.Draw(spriteBatch);

            foreach (NPC npc in npcs)
                npc.Draw(spriteBatch);
            
            gatherer.Draw(spriteBatch);

            foreach (Projectile p in projectiles)
                p.Draw(spriteBatch);

            storyTeller.Draw(spriteBatch);

            land.Draw(spriteBatch);

            graveFront.Draw(spriteBatch);

            death.Draw(spriteBatch);
        }
    }
}
