﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace bgj7
{
    class GameManager
    {
        ScreenManager screenManager;
        public AudioManager audioManager;
        ContentManager content;
        GraphicsDevice graphics;
        public Player Player;
        public StoryTeller storyTeller;

        public Random rand;

        /// <summary>
        /// Includes 
        /// </summary>
        public GameManager(ContentManager content, GraphicsDevice graphics)
        {
            this.audioManager = new AudioManager();
            this.Player = new Player(this);
            this.content = content;
            this.screenManager = new ScreenManager(this);
            this.graphics = graphics;
            this.rand = new Random();

            screenManager.ChangeScreen("intro");
        }

        public void AssignAudioManager(AudioManager audioManager)
        {
            this.audioManager = audioManager;
        }

        public void AssignStoryTeller(StoryTeller storyTeller)
        {
            this.storyTeller = storyTeller;
        }

        public void Update(GameTime gameTime)
        {
            Player.Update(gameTime);
            screenManager.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            screenManager.Draw(spriteBatch);
        }

        public Texture2D GetTexture2D(string name)
        {
            return content.Load<Texture2D>(name);
        }

        public SpriteFont GetFont(string name)
        {
            return content.Load<SpriteFont>(name);
        }

        public void ClearScreen(Color color)
        {
            graphics.Clear(color);
        }

        public void ChangeScreen(string name)
        {
            screenManager.ChangeScreen(name);
        }
    }
}
