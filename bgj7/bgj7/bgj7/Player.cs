﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace bgj7
{
    class Player
    {
        GameManager game;
        KeyboardState lastFrameState;
        KeyboardState currentFrameState;

        GamePadState lastFramePadState;
        GamePadState currentFramePadState;


        public Player(GameManager game)
        {
            this.game = game;

            lastFrameState = Keyboard.GetState();
        }

        public void Update(GameTime gameTime)
        {
            lastFrameState = currentFrameState;

            currentFrameState = Keyboard.GetState();

            lastFramePadState = currentFramePadState;

            currentFramePadState = GamePad.GetState(PlayerIndex.One);
        }

        public bool WasKeyPressed(Keys key)
        {
            return (currentFrameState.IsKeyDown(key) &&
                    lastFrameState.IsKeyUp(key));
        }

        public bool WasKeyReleased(Keys key)
        {
            return (lastFrameState.IsKeyDown(key) &&
                    currentFrameState.IsKeyUp(key));
        }

        public bool WasButtonPressed(Buttons button)
        {
            return (currentFramePadState.IsButtonDown(button) &&
                    lastFramePadState.IsButtonUp(button));
        }

        public bool WasButtonReleased(Buttons button)
        {
            return (lastFramePadState.IsButtonDown(button) &&
                    currentFramePadState.IsButtonUp(button));
        }

        public bool IsButtonPressed(Buttons button)
        {
            return currentFramePadState.IsButtonDown(button);
        }

        public bool IsKeyPressed(Keys key)
        {
            return currentFrameState.IsKeyDown(key);
        }

        public bool PressingRight()
        {
            return (IsButtonPressed(Buttons.DPadRight) ||
                    IsButtonPressed(Buttons.LeftThumbstickRight) ||
                    IsKeyPressed(Keys.D) ||
                    IsKeyPressed(Keys.Right));
        }

        public bool PressingLeft()
        {
            return (IsButtonPressed(Buttons.DPadLeft) ||
                    IsButtonPressed(Buttons.LeftThumbstickLeft) ||
                    IsKeyPressed(Keys.A) ||
                    IsKeyPressed(Keys.Left));
        }

        public bool PressingDown()
        {
            return (IsButtonPressed(Buttons.DPadDown) ||
                    currentFramePadState.ThumbSticks.Left.Y < -0.5f ||
                    IsKeyPressed(Keys.S) ||
                    IsKeyPressed(Keys.Down));
        }

        public bool PressedEnter()
        {
            return (WasButtonReleased(Buttons.Start) ||
                    WasKeyReleased(Keys.Space) ||
                    WasKeyReleased(Keys.Enter));
        }

        public bool Jumped()
        {
            return (IsButtonPressed(Buttons.A) ||
                    IsKeyPressed(Keys.Space) ||
                    IsKeyPressed(Keys.Up) ||
                    IsKeyPressed(Keys.W));
        }
    }
}
