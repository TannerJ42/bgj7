﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace bgj7
{
    class StoryTeller
    {
        GameManager game;
        Player player;
        Gatherer gatherer;
        Death death;
        List<Platform> platforms;
        List<Projectile> projectiles;

        Story currentStory;
        List<Story> storyQueue;

        Dictionary<string, Story> stories;
        Dictionary<string, string> strings;
        Dictionary<string, bool> storyStatus;

        SpriteFont font;

        bool playAudio = true;

        float timer = 20;

        public bool level10 = false;
        public bool gameOver;

        public bool returnToTitle;

        Cue bgm;
        Cue nextBGM;

        bool nextBGMLoaded = false;

        List<string> randomCues = new List<string>()
        {
            "dx_DeathRandom01_01",
            "dx_DeathRandom01_02",
            "dx_DeathRandom01_03",
            "dx_DeathRandom01_04",
            "dx_DeathRandom01_05",
            "dx_DeathRandom01_06",
            "dx_DeathRandom01_07",
            "dx_DeathRandom01_08",
            "dx_DeathRandom01_09",

        };

        public StoryTeller(GameManager game, Player player, Gatherer gatherer, Death death, ref List<Platform> platforms, ref List<Projectile> projectiles)
        {
            this.game = game;
            this.player = player;
            this.gatherer = gatherer;
            this.death = death;
            this.platforms = platforms;
            this.projectiles = projectiles;

            font = game.GetFont("largeStory");

            storyQueue = new List<Story>();
            stories = new Dictionary<string, Story>();
            storyStatus = new Dictionary<string, bool>();

            PopulateStrings();
            PopulateStories();
        }

        void PopulateStrings()
        {
            strings = new Dictionary<string, string>()
            {
{"dx_DeathStart01", "I\'ve waited too long for your craving to drive you out of hiding, Nitocris.\nFor your long sleep to end. Though you may think yourself immortal, I am Inevitable.\nNow flee, vampyr. See how long you can keep me at bay."},
 
{"dx_DeathSee01", "Ah, so you\'ve found unfortunate prey."},
 
{"dx_DeathFeed01", "You only delay the inevitable."},
 
{"dx_DeathMiss01", "Oh? Perhaps you welcome me?"},
 
{"dx_DeathAppear01", "You\'ve cheated me long enough, vampyr."},
 
{"dx_DeathHit01", "Your age is showing."},

{"dx_DeathWin01", "Hm, Hm, Hm, Hmm - at last."},

{"dx_DeathLose01", "So. You extended your misery. Well done. Sweet dreams, Methuselah."}, 

{"dx_DeathRandom01_01", "Yes, run. Let\'s revel in our final dance."},

{"dx_DeathRandom01_02", "As you starve, we grow closer."},

{"dx_DeathRandom01_03", "I\'ve searched eternity for you."}, 
 
{"dx_DeathRandom01_04", "I will consume you."},
 
{"dx_DeathRandom01_05", "Enough with the petty tricks."},
 
{"dx_DeathRandom01_06", "Beware coward."},

{"dx_DeathRandom01_07", "Death will always be waiting."}, 

{"dx_DeathRandom01_08", "I sense your fear."},

{"dx_DeathRandom01_09", "I\'ll have you, as I have your brother."}, 

{"dx_DeathRandom01_10", "Your struggle is meaningless."},

{"dx_DeathRandom01_11", "The void beckens."},

{"dx_DeathRandom01_12", "Such senseless sacrifice to serve such as you."},

{"dx_DeathRandom01_13", "Without me, you have no meaning."},

{"dx_DeathRandom01_14", "The struggle makes the inevitable sweeter."},

{"dx_DeathRandom01_15", "You can cheat your people, but you cannot cheat Death."},

{"dx_DeathRandom01_16", "There\'s nothing left alive to mourn you."},
            };

            Console.WriteLine("Writing strings.");
        }

        void PopulateStories()
        {
            //CreateStory("dx_DeathStart01");
            //CreateStory("dx_DeathStart01");
            foreach (string s in strings.Keys)
            {
                CreateStory(s);
            }

            foreach (Story s in stories.Values)
            {
                
                storyStatus.Add(s.name, false);
            }
        }

        void CreateStory(string name)
        {
            Story story = new Story(name, font, strings[name], game.audioManager.GetCue(name));
            stories.Add(name, story);
        }

        public void TellStory(string name)
        {
            try
            {
                storyQueue.Add(stories[name]);
                storyStatus[name] = true;
                //stories.Remove(name);
            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("Couldn't find key " + name);
            }
        }

        public void Update(GameTime gameTime)
        {
            //if (currentStory != null)
            //    Console.WriteLine(storyStatus[currentStory.name]);

            timer -= (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (timer <= 0)
            {
                TellStory(randomCues[game.rand.Next(randomCues.Count)]);
                timer = 30;
            }

            if (currentStory != null)
            {
                currentStory.Update(gameTime);
                if (currentStory.state == Story.StoryState.Finished)
                {
                    currentStory = null;
                }
            }
            else
            {
                if (storyQueue.Count > 0)
                {
                    currentStory = storyQueue[0];
                    currentStory.RefreshAudio(game.audioManager);
                    
                    storyQueue.RemoveAt(0);
                    Console.WriteLine("Making current story " + currentStory.name + ".");
                }
            }

            if (!HasPlayed("dx_DeathAppear01") &&
                projectiles.Count > 0)
            {
                TellStory("dx_DeathAppear01");
            }

            if (!HasPlayed("dx_DeathFeed01") &&
                gatherer.IsFeeding)
            {
                TellStory("dx_DeathFeed01");
            }

            if (!HasPlayed("dx_DeathHit01") &&
                gatherer.IsStunned)
            {
                TellStory("dx_DeathHit01");
            }

            if (bgm != null &&
                !bgm.IsPlaying)
            {
                if (gatherer.alive)
                {
                    if (playAudio)
                    {
                        bgm = GetNextLoop();
                        bgm.Play();
                    }
                    else
                        returnToTitle = true;
                }
                else
                {

                }
            }
            //if (gatherer.IsFeeding &&
            //    !HasPlayed("dx_DeathStart01"))
            //{
            //    TellStory("dx_DeathStart01");
            //}
        }

        public bool HasPlayed(string name)
        {
            return storyStatus[name];
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (currentStory != null)
                currentStory.Draw(spriteBatch);
        }

        Cue GetNextLoop()
        {
            Cue cue = null;

            if (!gameOver)
            {
                Console.WriteLine("Choosing new loop.");
                if (death.drawRectangle.Right < 300)
                {
                    cue = game.audioManager.GetCue("BGM_Int1");
                }
                else if (death.drawRectangle.Right < 400)
                {
                    cue = game.audioManager.GetCue("BGM_Int2");
                }
                else if (death.drawRectangle.Right < 500)
                {
                    cue = game.audioManager.GetCue("BGM_Int3");
                }
                else if (death.drawRectangle.Right < 800)
                {
                    cue = game.audioManager.GetCue("BGM_Int4");
                }
                else
                {
                    cue = game.audioManager.GetCue("BGM_Int5");
                }

                Console.WriteLine("Chose " + cue.Name);
            }
            else if (playAudio)
            {
                cue = game.audioManager.GetCue("BGM_PlayerWin");
                playAudio = false;
            }
            //else if (level10)
            //{
            //    gameOver = true;
            //    cue = game.audioManager.GetCue("BGM_Int5");
            //}

            return cue;
        }

        internal void PlayBGM(string name)
        {
            bgm = game.audioManager.GetCue(name);
            bgm.Play();
        }

        public void StopAudio()
        {
            if (currentStory != null)
                currentStory.audio.Stop(AudioStopOptions.Immediate);
        }
    }
}
