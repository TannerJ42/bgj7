﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace bgj7
{
    class Story
    {
        public StoryState state = StoryState.Appearing;

        public string name;
        public Color fontColor;
        public string message;
        Vector2 location;
        public Cue audio;

        SpriteFont font;

        public enum StoryState
        {
            Appearing,
            Playing,
            Fading,
            Finished
        }

        public Story(string name, SpriteFont font, string message, Cue audio)
        {
            this.name = name;
            this.font = font;
            this.message = message;
            this.audio = audio;
            location = new Vector2(CONSTANTS.message_x, CONSTANTS.message_y);

            fontColor = new Color(255, 255, 255, 0);
        }

        public void RefreshAudio(AudioManager audioManager)
        {
            this.audio = audioManager.GetCue(name);
            state = StoryState.Appearing;
            fontColor = new Color(255, 255, 255, 0);
        }

        internal void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(font, message, location, fontColor);
        }

        internal void Update(GameTime gameTime)
        {
            if (state == StoryState.Appearing)
            {
                if (fontColor.A < 245)
                {
                    fontColor.A += 5;
                }
                else
                {
                    fontColor.A = 255;
                    state = StoryState.Playing;
                    audio.Play();
                }
            }
            else if (state == StoryState.Playing)
            {
                if (audio.IsStopped)
                {
                    state = StoryState.Fading;
                    Console.WriteLine("Cue stopped.");
                }
            }
            else if (state == StoryState.Fading)
            {
                if (fontColor.A <= 0)
                {
                    fontColor.A -= 10;
                }
                else
                {
                    fontColor.A = 0;
                    state = StoryState.Finished;
                }
            }
        }
    }
}
