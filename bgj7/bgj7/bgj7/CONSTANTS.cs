﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bgj7
{
    class CONSTANTS
    {
        public const int screen_width = 1280;
        public const int screen_height = 720;

        public const int jump_height = -500;
        public const float slowdown_coefficient = 0.5f;
        public const float jumping_slowdown_coefficient = 0.75f;

        public const float speed_limit = 370;

        public const int sprite_width = 70;
        public const int sprite_height = 120;

        public const int left_border = 0;
        public const int right_border = 1000 - sprite_width; // screen starts to scroll heergit
        public const int top_border = 0;
        public const int bottom_border = 690 - sprite_height;

        public const int projectile_spawn_max_y = 700;
        public const int projectile_spawn_min_y = 200;

        public const int platform_spawn_max_y = 700;
        public const int platform_spawn_min_y = 200;

        public const int vampire_frame_width = 320;
        public const int vampire_frame_height = 470;
        public const int vampire_frames = 6;

        public const int vampire_draw_width = 80;
        public const int vampire_draw_height = 235 / 2;

        public const int vampire_crouch_hitbox_offset_x = 32;
        public const int vampire_crouch_hitbox_offset_y = 40;
        public const int vampire_crouch_hitbox_width = 30;
        public const int vampire_crouch_hitbox_height = 130 / 2;

        public const int vampire_hitbox_offset_x = 36;
        public const int vampire_hitbox_offset_y = 12;
        public const int vampire_hitbox_width = 25;
        public const int vampire_hitbox_height = 195 / 2;

        public const int projectile_hitbox_offset_x = 12;
        public const int projectile_hitbox_offset_y = 11;
        public const int projectile_hitbox_width = 75;
        public const int projectile_hitbox_height = 210 / 4;

        public const float feeding_time = 3f;

        public const int death_offset = 1100;

        public const int message_x = 10;
        public const int message_y = 10;

        public static Dictionary<string, string> strings = new Dictionary<string, string>()
        {

        };
    }
}
