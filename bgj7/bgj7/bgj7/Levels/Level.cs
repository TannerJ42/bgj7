﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bgj7
{
    class Level
    {
        public float distance;
        public int platformYMin;
        public int platformYMax;
        public int platformSpawnPeriod;
        public float projectileSpawnPeriod;
        public int projectileYMin;
        public int projectileYMax;
        public int chanceForAnotherPlatform;
        public int chanceForAnotherProjectile;
        public int chanceForNPC;
        public int maxNPCs;

        public Level(int distance, int platformYMin, int platformYMax, int platformSpawnPeriod, int chanceForAnotherPlatform,
            int projectileYMin, int projectileYMax, float projectileSpawnPeriod, int chanceForAnotherProjectile,
            int chanceForNPC, int maxNPCs)
        {
            this.distance = distance;
            this.platformYMin = platformYMin;
            this.platformYMax = platformYMax;
            this.platformSpawnPeriod = platformSpawnPeriod;
            this.projectileYMin = projectileYMin;
            this.projectileYMax = projectileYMax;
            this.projectileSpawnPeriod = projectileSpawnPeriod;
            this.chanceForAnotherPlatform = chanceForAnotherPlatform;
            this.chanceForAnotherProjectile = chanceForAnotherProjectile;
            this.chanceForNPC = chanceForNPC;
            this.maxNPCs = maxNPCs;
        }
    }
}
