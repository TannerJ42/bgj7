﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bgj7
{
    class Levels
    {
        public static int[] distances = new int[11]
        {
            500, 1000, 1700, 2400,3100, 3800, 4500, 6000, 6000, 2000, 1280
        };

        public static int[] platformMinY = new int[11] //lower is higher 0 is top 
        {
            550, 500, 450, 400, 350, 300, 250, 200, 150, 100, 100
        };

        public static int[] platformMaxY = new int[11]
        {
            650, 650, 650, 650, 650, 650, 650, 650, 650, 650, 650
        };

        public static int[] platformSpawnPeriod = new int[11]
        {
            500, 500, 500, 400, 400, 400, 350, 350, 300, 10000, 10000
        };

        public static int[] chanceForAnotherPlatform = new int[11]
        {
            20, 22, 24, 26, 28, 30, 32, 34, 40, 0, 0
        };

        public static int[] projectileMinY = new int[11]
        {
            550, 500, 450, 400, 350, 300, 250, 200, 150, 100, 100
        };

        public static int[] projectileMaxY = new int[11]
        {
            640, 640, 640, 640, 640, 640, 640, 640, 640, 640, 640
        };

        public static float[] projectileSpawnPeriod = new float[11]
        {
            8, 8, 7, 7, 6, 6, 5, 5, 4, 100, 100
        };

        public static int[] chanceForAnotherProjectile = new int[11]
        {
            0, 0, 15, 15, 20, 20, 25, 25, 25, 0, 0
        };

        public static int[] chanceForNPC = new int[11]
        {
            100, 100, 25, 25, 20, 20, 20, 20, 20, 0, 0
        };

        public static int[] maxNPCs = new int[11]
        {
            1, 1, 2, 3, 4, 5, 6, 7, 8, 0, 0
        };
    }
}
