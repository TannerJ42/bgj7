﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace bgj7
{
    class Gatherer
    {
        public bool alive = true;
        public bool visible = true;

        GameManager game;
        Player player;
        Sprite sprite;
        Sprite feedingSprite;
        Sprite boyFeedingSprite;
        Sprite girlFeedingSprite;
        testScreen screen;

        public bool moveForward;
        public float moveForwardSpeed;

        Death death;

        Texture2D pixel;

        List<Projectile> projectiles;

        float speed = 5;
        float pushDeathAmount = 20f;

        NPC feedingNPC;

        //bool jumping = false;
        bool canJump = false;
        bool hasDoubleJumped = false;
        bool canDoubleJump = false;

        float runningFrameTimer;

        int lastRoundBottom = 0;

        //Vector2 position;
        Vector2 velocity;
        readonly Vector2 gravity = new Vector2(0, 100 * 9.89f);

        float jumpingTimer = 0;

        float stunTimer = 0;

        float fallingTimer = 0;

        State state = State.running;

        float feedingTimer = 0;

        bool drawHitRectangle = false;

        public bool CanMove = false;

        public bool gameOver = false;

        enum State
        {
            jumping,
            stunned,
            running,
            crouching,
            feeding,
        }

        public Rectangle hitRectangle
        {
            get
            {
                if (!(state == State.jumping ||
                    state == State.crouching))
                {
                    return new Rectangle(sprite.drawRectangle.X + CONSTANTS.vampire_hitbox_offset_x, sprite.drawRectangle.Y + CONSTANTS.vampire_hitbox_offset_y,
                                        CONSTANTS.vampire_hitbox_width, CONSTANTS.vampire_hitbox_height);
                }
                else
                {
                    return new Rectangle(sprite.drawRectangle.X + CONSTANTS.vampire_crouch_hitbox_offset_x, sprite.drawRectangle.Y + CONSTANTS.vampire_crouch_hitbox_offset_y,
                                        CONSTANTS.vampire_crouch_hitbox_width, CONSTANTS.vampire_crouch_hitbox_height);
                }
            }
        }

        private Rectangle feetRectangle
        {
            get { return new Rectangle(sprite.drawRectangle.X, lastRoundBottom, sprite.drawRectangle.Width, sprite.drawRectangle.Bottom - lastRoundBottom); }
        }

        private Rectangle feedRectangle
        {
            get { return new Rectangle(sprite.drawRectangle.X, sprite.drawRectangle.Bottom - 5, hitRectangle.Width, 1); }
        }

        public Gatherer(Player player, GameManager game, testScreen screen)
        {
            this.player = player;
            this.game = game;
            this.screen = screen;
            sprite = new Sprite(game.GetTexture2D("vampire_animation"), new Vector2(0, 1000));
            boyFeedingSprite = new Sprite(game.GetTexture2D("vampire_feed"), new Vector2(0, 1000));
            girlFeedingSprite = new Sprite(game.GetTexture2D("vampire_feed2"), new Vector2(0, 1000));
            sprite.position = new Vector2(sprite.drawRectangle.X, sprite.drawRectangle.Y);
            sprite.drawRectangle.Width = CONSTANTS.vampire_draw_width;
            sprite.drawRectangle.Height = CONSTANTS.vampire_draw_height;
            sprite.sourceRectangle.Width = sprite.sourceRectangle.Width / CONSTANTS.vampire_frames;
            //feedingSprite = boyFeedingSprite;
            boyFeedingSprite.sourceRectangle.Width = sprite.sourceRectangle.Width - 10;
            boyFeedingSprite.sourceRectangle.Height = sprite.sourceRectangle.Height;
            boyFeedingSprite.drawRectangle.Width = sprite.drawRectangle.Width;
            boyFeedingSprite.drawRectangle.Height = sprite.drawRectangle.Height;

            girlFeedingSprite.sourceRectangle.Width = sprite.sourceRectangle.Width - 10;
            girlFeedingSprite.sourceRectangle.Height = sprite.sourceRectangle.Height;
            girlFeedingSprite.drawRectangle.Width = sprite.drawRectangle.Width;
            girlFeedingSprite.drawRectangle.Height = sprite.drawRectangle.Height;
            velocity = new Vector2();

            pixel = game.GetTexture2D("pixel");
        }

        public void SetReferences(Death death, ref List<Projectile> projectiles)
        {
            this.death = death;
            this.projectiles = projectiles;
        }

        public void Update(GameTime gameTime, List<Platform> platforms, List<NPC> npcs, Death death)
        {

            float time = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (alive)
            {

                lastRoundBottom = sprite.drawRectangle.Bottom;

                if (stunTimer <= 0 ||
                    state == State.feeding)
                {
                    if (player.PressingRight())
                    {
                        if (CanMove)
                        {
                            if (state == State.running)
                                Move(1, 0);
                            if (sprite.facingLeft)
                                sprite.FaceRight();
                        }
                    }
                    else if (velocity.X > 0)
                    {
                        if (!canJump)
                            velocity.X *= CONSTANTS.jumping_slowdown_coefficient;
                        else
                            velocity.X *= CONSTANTS.slowdown_coefficient;
                    }
                    if (player.PressingLeft())
                    {
                        if (CanMove)
                        {
                            if (state == State.running)
                                Move(-1, 0);
                            if (!sprite.facingLeft)
                                sprite.FaceLeft();
                        }
                    }
                    else if (velocity.X < 0)
                    {
                        if (!canJump)
                            velocity.X *= CONSTANTS.jumping_slowdown_coefficient;
                        else
                            velocity.X *= CONSTANTS.slowdown_coefficient;
                    }

                    if (canJump &&          // jump
                        player.Jumped() &&
                        CanMove &&
                        state != State.feeding)
                    {
                        if (!player.PressingDown())
                            Jump();
                        else
                            JumpDown();
                    }
                    if (!canJump &&        // turn on double jump
                        !hasDoubleJumped)// &&
                    //jumpingTimer <= 0)
                    {
                        //if (!player.IsButtonPressed(Buttons.A) &&
                        //    !player.IsKeyPressed(Keys.Space) &&
                        //    !player.IsKeyPressed(Keys.Up))
                        if (!player.Jumped())
                            canDoubleJump = true;
                    }
                    if (!canJump &&       // double jump
                        canDoubleJump &&
                        player.Jumped() &&
                        CanMove)
                    {
                        DoubleJump();
                    }
                    if (canJump &&
                        player.PressingDown() &&
                        CanMove)
                    {
                        Crouch();
                    }
                    if (state == State.crouching)
                    {
                        velocity.X *= 0.9f;
                        if (!player.PressingDown())
                        {
                            StopCrouch();
                        }
                    }
                }
                else
                {
                    velocity.X *= 0.95f;
                }

                if (stunTimer > 0)
                    stunTimer -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                else if (state == State.stunned)
                    state = State.running;
                if (jumpingTimer > 0)
                    jumpingTimer -= (float)gameTime.ElapsedGameTime.TotalSeconds;

                if (fallingTimer > 0)
                    fallingTimer -= (float)gameTime.ElapsedGameTime.TotalSeconds;

                if (moveForward)
                    velocity.X = moveForwardSpeed;

                if (CanMove)
                    velocity += gravity * time;
                else
                    velocity += new Vector2(0, gravity.X * time);
                sprite.position += velocity * time;

                sprite.drawRectangle.X = (int)sprite.position.X;
                sprite.drawRectangle.Y = (int)sprite.position.Y;

                foreach (Projectile p in projectiles)
                {
                    if (p.CheckForCollision(hitRectangle))
                    {
                        GetHitByProjectile();
                    }
                }

                if (sprite.drawRectangle.Bottom > lastRoundBottom &&
                    fallingTimer <= 0)
                {

                    foreach (Platform p in platforms)
                    {
                        if (p.topBox.Intersects(feetRectangle))
                        {
                            sprite.drawRectangle.Y = p.sprite.drawRectangle.Y - sprite.drawRectangle.Height;
                            sprite.position.Y = p.sprite.drawRectangle.Y - sprite.drawRectangle.Height;
                            Landed();
                        }
                    }
                }

                if (sprite.drawRectangle.Y > CONSTANTS.bottom_border)
                {
                    sprite.drawRectangle.Y = CONSTANTS.bottom_border;
                    sprite.position.Y = CONSTANTS.bottom_border;
                    Landed();
                    //jumping = false;
                }
                if (sprite.drawRectangle.Y < CONSTANTS.top_border)
                {
                    sprite.drawRectangle.Y = CONSTANTS.top_border;
                    sprite.position.Y = CONSTANTS.top_border;
                    //jumping = false;
                }
                if (sprite.drawRectangle.X > CONSTANTS.right_border &&
                    !gameOver)
                {
                    float difference = sprite.drawRectangle.X - CONSTANTS.right_border;

                    sprite.position.X = CONSTANTS.right_border;

                    screen.Scroll(difference);
                }

                if (sprite.drawRectangle.X < CONSTANTS.left_border)
                {
                    sprite.position.X = CONSTANTS.left_border;
                }

                if (state == State.running)
                {
                    foreach (NPC n in npcs)
                    {
                        if (this.feedRectangle.Intersects(n.drawRectangle) &&
                            n.alive &&
                            n.visible &&
                            !IsFeeding)
                        {
                            Feed(n);
                        }
                    }
                }

                if (state == State.feeding)
                {
                    death.Move(pushDeathAmount);
                    feedingTimer -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                    if (feedingTimer < 0)
                        StopFeeding(feedingNPC);
                    else
                    {
                        if (feedingTimer >= CONSTANTS.feeding_time * 0.75f)
                        {

                            feedingSprite.sourceRectangle.X = feedingSprite.image.Width / 2;
                        }
                        else
                        {
                            if (feedingSprite.sourceRectangle.X != 0)
                            {
                                feedingSprite.sourceRectangle.X = 0;
                                if (!feedingNPC.girl)
                                    game.audioManager.PlayCue("vx_HumanDeath");
                                else
                                    game.audioManager.PlayCue("vx_GirlDeath");
                                //game.audioManager.PlayCue("vx_HumanDeath");
                            }
                        }
                        //if (game.rand.Next(200) == 42)
                        //{
                        //    feedingSprite.sourceRectangle.X += feedingSprite.image.Width / 2;
                        //    if (feedingSprite.sourceRectangle.X == feedingSprite.sourceRectangle.Width)
                        //        feedingSprite.sourceRectangle.X = 0;
                        //}
                    }
                }

                if (death.deathLine >= hitRectangle.Left)
                {
                    game.audioManager.PlayCue("Thirst_Bell");
                    alive = false;
                    visible = false;
                    death.Win();
                }
            }

            CalculateFrame(gameTime);
        }

        void CalculateFrame(GameTime gameTime)
        {
            if (state == State.stunned)
            {
                sprite.sourceRectangle.X = 5 * CONSTANTS.vampire_frame_width;
            }
            else if (state == State.jumping)  // jumping
            {
                sprite.sourceRectangle.X = 4 * CONSTANTS.vampire_frame_width;
            }
            else if (state == State.crouching)
            {
                sprite.sourceRectangle.X = 3 * CONSTANTS.vampire_frame_width;
            }
            else
            {
                if ((int)velocity.X == 0)  // standing frame
                    sprite.sourceRectangle.X = 0 * CONSTANTS.vampire_frame_width;
                else
                {
                    ChooseRunningFrame(gameTime);
                }
            }
        }

        void ChooseRunningFrame(GameTime gameTime)
        {
            if (!(sprite.sourceRectangle.X == 2 * CONSTANTS.vampire_frame_width ||
                sprite.sourceRectangle.X == 1 * CONSTANTS.vampire_frame_width))
            {
                sprite.sourceRectangle.X = 1 * CONSTANTS.vampire_frame_width;
                runningFrameTimer = 0.2f;
            }
            else
            {
                runningFrameTimer -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                if (runningFrameTimer < 0)
                {
                    ChangeRunningFrame();
                    runningFrameTimer = 0.2f;
                }
                
            }
        }

        void ChangeRunningFrame()
        {
            if (sprite.sourceRectangle.X == 1 * CONSTANTS.vampire_frame_width)
                sprite.sourceRectangle.X = 2 * CONSTANTS.vampire_frame_width;
            else
                sprite.sourceRectangle.X = 1 * CONSTANTS.vampire_frame_width;
        }

        void Jump()
        {
            if (state != State.feeding)
            {
                game.audioManager.PlayCue("vx_NitocrisJump");

                canJump = false;
                velocity.Y = CONSTANTS.jump_height;
                jumpingTimer = 0.5f;
                state = State.jumping;

                if (player.PressingLeft())
                {
                    if (velocity.X > 0)
                        velocity.X = 0;
                    Move(-50, 0);
                    if (!sprite.facingLeft)
                        sprite.FaceLeft();
                }
                if (player.PressingRight())
                {
                    if (velocity.X < 0)
                        velocity.X = 0;
                    Move(50, 0);
                    if (sprite.facingLeft)
                        sprite.FaceRight();
                }
            }
        }

        void JumpDown()
        {
            jumpingTimer = 0.05f;
            fallingTimer = 0.1f;
            canJump = false;
            state = State.jumping;
        }

        void Feed(NPC npc)
        {
            game.audioManager.PlayCue("vx_NitocrisFeed");

            if (npc.girl)
                feedingSprite = girlFeedingSprite;
            else
                feedingSprite = boyFeedingSprite;

            this.sprite.position.X = npc.position.X;
            this.sprite.position.Y = npc.position.Y - sprite.drawRectangle.Height;//npc.position.Y + npc.drawRectangle.Height - sprite.drawRectangle.Height;

            this.sprite.drawRectangle.X = (int)sprite.position.X;
            this.sprite.drawRectangle.Y = (int)sprite.position.Y;

            npc.visible = false;
            state = State.feeding;
            feedingTimer = CONSTANTS.feeding_time;

            feedingNPC = npc;

            feedingSprite.position.X = sprite.drawRectangle.X;
            feedingSprite.position.Y = sprite.drawRectangle.Y;

            this.velocity.X = 0;
            this.velocity.Y = 0;
        }

        void StopFeeding(NPC npc)
        {
            npc.Kill();
            feedingNPC = null;
            state = State.running;
        }

        void Crouch()
        {
            if (state != State.feeding)
                state = State.crouching;
        }

        void StopCrouch()
        {
            state = State.running;
        }

        void DoubleJump()
        {
            game.audioManager.PlayCue("vx_NitocrisDoubleJump");

            hasDoubleJumped = true;
            canDoubleJump = false;
            velocity.Y = CONSTANTS.jump_height;

            if (player.PressingLeft())
            {
                if (velocity.X > 0)
                    velocity.X = 0;
                Move(-50, 0);
                if (!sprite.facingLeft)
                    sprite.FaceLeft();
            }
            if (player.PressingRight())
            {
                if (velocity.X < 0)
                    velocity.X = 0;
                Move(50, 0);
                if (sprite.facingLeft)
                    sprite.FaceRight();
            }
        }

        void Landed()
        {
            canJump = true;
            velocity.Y = 0;
            canDoubleJump = false;
            hasDoubleJumped = false;
            if (state == State.jumping)
                state = State.running;
        }

        public void Move(float X, float Y)
        {
            velocity += new Vector2(X, Y) * speed;

            if (velocity.X > CONSTANTS.speed_limit)
                velocity.X = CONSTANTS.speed_limit;
            else if (velocity.X < -CONSTANTS.speed_limit)
                velocity.X = -CONSTANTS.speed_limit;
        }

        public void GetHitByProjectile()
        {
            velocity.X = -300;
            stunTimer = 0.9f; // seconds
            if (feedingNPC != null)
                StopFeeding(feedingNPC);
            state = State.stunned;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (visible)
            {

                if (state == State.feeding)
                    feedingSprite.Draw(spriteBatch);
                else
                    sprite.Draw(spriteBatch);
            }
            if (drawHitRectangle)
            {
                spriteBatch.Draw(pixel, hitRectangle, new Color(0, 0, 0, 50));
                foreach (Projectile p in projectiles)
                    spriteBatch.Draw(pixel, p.hitRectangle, new Color(0, 0, 0, 50));
            }
        }

        public bool IsFeeding
        {
            get { return state == State.feeding; }
        }

        public bool IsStunned
        {
            get { return state == State.stunned; }
        }

        internal void Ground()
        {
            velocity.Y += 10;
        }
    }
}
