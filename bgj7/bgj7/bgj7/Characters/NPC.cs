﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace bgj7
{
    class NPC
    {
        public bool alive = true;

        public Sprite deadSprite;

        GameManager game;
        Texture2D image;
        public Rectangle drawRectangle;
        Rectangle sourceRectangle;
        public bool visible = true;

        public Vector2 position;

        SpriteEffects facing;

        public bool girl;

        bool playedAudio = false;

        public NPC(GameManager game, Platform platform)
        {
            this.game = game;


            if (game.rand.Next(1, 3) == 2)
            {
                girl = true;
                image = game.GetTexture2D("npc2_animation");
                deadSprite = new Sprite(game.GetTexture2D("npc2_dead"), new Vector2());
            }
            else
            {
                image = game.GetTexture2D("npc_animation");
                deadSprite = new Sprite(game.GetTexture2D("npc_dead"), new Vector2());
            }

            if (game.rand.Next(1, 3) == 2)
                facing = SpriteEffects.FlipHorizontally;
            else
                facing = SpriteEffects.None;

            //image = game.GetTexture2D("npc_animation");
            drawRectangle = new Rectangle(0, 0, (image.Width / 4) / 2, image.Height / 4);
            sourceRectangle = new Rectangle(0, 0, image.Width / 2, image.Height);

            position = new Vector2(platform.sprite.position.X + game.rand.Next(0, platform.sprite.drawRectangle.Width - drawRectangle.Width), platform.sprite.position.Y);
            drawRectangle.Y = (int)platform.sprite.position.Y - drawRectangle.Height;

            
            deadSprite.drawRectangle.Width = deadSprite.drawRectangle.Width / 4;
            deadSprite.drawRectangle.Height = deadSprite.drawRectangle.Height / 4;
        }

        public void Update(GameTime gameTime)
        {
            if (alive && visible)
            {
                if (!playedAudio &&
                    position.X + 100 <= CONSTANTS.screen_width)
                {
                    if (!girl)
                        game.audioManager.PlayCue("vx_HumanWhimper");
                    else
                        game.audioManager.PlayCue("vx_GirlWhimper");
                    playedAudio = true;
                }

                if (game.rand.Next(200) == 42)
                {
                    if (facing == SpriteEffects.FlipHorizontally)
                        facing = SpriteEffects.None;
                    else
                        facing = SpriteEffects.FlipHorizontally;
                }
                if (game.rand.Next(200) == 42)
                {
                    sourceRectangle.X += sourceRectangle.Width;
                    sourceRectangle.X %= sourceRectangle.Width * 2;
                }
            }
        }

        public void Kill()
        {
            alive = false;
            visible = true;

            deadSprite.position.X = drawRectangle.X;
            deadSprite.drawRectangle.X = drawRectangle.X;
            deadSprite.position.Y = drawRectangle.Y;
            deadSprite.drawRectangle.Y = drawRectangle.Y;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            drawRectangle.X = (int)position.X;

            if (visible)
            {
                if (alive)
                    spriteBatch.Draw(image, drawRectangle, sourceRectangle, Color.White, 0f, new Vector2(), facing, 0);
                else
                {
                    deadSprite.Draw(spriteBatch);
                }
            }
        }
    }
}
