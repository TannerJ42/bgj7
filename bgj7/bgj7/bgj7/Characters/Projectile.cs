﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace bgj7
{
    class Projectile
    {
        GameManager game;

        public Vector2 position;
        public Rectangle drawRectangle;
        Texture2D image;

        bool hitPlayer = false;

        bool stopped = true;

        float stopTimer = 0.6f;

        float speed = 240;

        int alpha = 255;

        bool playedPassingAudio = false;

        public Rectangle hitRectangle
        {
            get { return new Rectangle(drawRectangle.X + CONSTANTS.projectile_hitbox_offset_x, drawRectangle.Y + CONSTANTS.projectile_hitbox_offset_y, 
                CONSTANTS.projectile_hitbox_width, CONSTANTS.projectile_hitbox_height); }
        }

        public Projectile(GameManager game, int location)
        {
            this.game = game;
            image = game.GetTexture2D("death_mini");
            drawRectangle = new Rectangle(CONSTANTS.screen_width, location, image.Width / 4, image.Height / 4);
            position = new Vector2(CONSTANTS.screen_width, location);
        }

        public void Update(GameTime gameTime, Gatherer gatherer)
        {
            position.X -= (float)(speed * gameTime.ElapsedGameTime.TotalSeconds);

            if (stopped &&
                position.X < 1200)
            {
                position.X = 1200;
                stopTimer -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                if (stopTimer <= 0)
                    stopped = false;
            }

            if (hitPlayer)
                alpha -= 10;

            if (!playedPassingAudio &&
                position.X < gatherer.hitRectangle.Left - 100)
            {
                if (game.rand.Next(100) <= 5)
                    game.audioManager.PlayCue("vx_GhostBacon");
                else
                    game.audioManager.PlayCue("vx_GhostWhoosh");
                playedPassingAudio = true;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            drawRectangle.X = (int)position.X;

            if (alpha > 0)
            {
                spriteBatch.Draw(image, drawRectangle, null, new Color(255, 255, 255, alpha), 0f, new Vector2(), SpriteEffects.FlipHorizontally, 0f);
            }
        }

        public bool CheckForCollision(Rectangle rect)
        {
            if (hitRectangle.Intersects(rect) &&
                !hitPlayer)
            {
                hitPlayer = true;
                return true;
            }

            return false;
        }
    }
}
