﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace bgj7
{
    class Death
    {
        bool won = false;

        int xOffset = -1280; // how far back death starts
        int yOffset = 0; // distance from top of screen to top of death
        int width = 1280;
        int height = 720;

        GameManager game;
        Texture2D image1;
        Texture2D image2;
        Texture2D image3;

        Texture2D pixel;

        Vector2 position;

        float sizeX;
        float sizeY;

        float speed = 57;

        float sizeMultipler = 1.0f;
        bool movingUp = true;

        public Rectangle drawRectangle;
        Rectangle drawRectangle2;
        Rectangle pixelRectangle;

        public bool GameLost
        {
            get
            {
                return won && pixelRectangle.Left > -100;
            }
        }

        public int deathLine
        {
            get { return drawRectangle.X + CONSTANTS.death_offset; }
        }



        public Death(GameManager game)
        {
            this.game = game;
            image1 = game.GetTexture2D("Death1");
            image2 = game.GetTexture2D("Death2");
            image3 = game.GetTexture2D("Death3");

            pixel = game.GetTexture2D("pixel");
            drawRectangle = new Rectangle(xOffset, yOffset, width, height);
            pixelRectangle = new Rectangle(0, 0, 1500, 720);
            position = new Vector2(xOffset, yOffset);
        }

        public void Update(GameTime gameTime)
        {
            position.X += (float)(speed * gameTime.ElapsedGameTime.TotalSeconds);

            if (position.X + width < 250)
            {
                position.X += 3;
                if (position.X + width > 250)
                    position.X = 250 - width;
            }

            //if (movingUp)
            //{
            //    sizeMultipler += 0.0005f;
            //    if (sizeMultipler > 1.05f)
            //    {
            //        movingUp = false;
            //    }

            //}
            //else
            //{
            //    sizeMultipler -= 0.0005f;
            //    if (sizeMultipler < 0.95f)
            //    {
            //        movingUp = true;
            //    }
            //}

            //drawRectangle2 = new Rectangle(drawRectangle.X, drawRectangle.Y, (int)(drawRectangle.Width * sizeMultipler), (int)(drawRectangle.Height * sizeMultipler));
            //drawRectangle2.X += drawRectangle.Width - drawRectangle2.Width;
            //drawRectangle2.Y += drawRectangle.Height - drawRectangle2.Height;

            ////drawRectangle2.Width += drawRectangle.Width - drawRectangle2.Width;
            ////drawRectangle2.Height += drawRectangle.Height - drawRectangle2.Height;
            pixelRectangle.X = (int)position.X - 1500;

        }

        public void Draw(SpriteBatch spriteBatch)
        {
            drawRectangle.X = (int)position.X;
            spriteBatch.Draw(image3, drawRectangle, Color.White);
            spriteBatch.Draw(image2, drawRectangle, Color.White);
            spriteBatch.Draw(image1, drawRectangle, Color.White);

            if (drawRectangle.Left > 0)
                spriteBatch.Draw(pixel, pixelRectangle, Color.Black);
        }

        public void Move(float amount)
        {
            position.X -= amount * 0.1f;
        }

        public void Reset()
        {
            this.position.X = 0 - drawRectangle.Width;
        }

        internal void Win()
        {
            this.speed *= 20;
            won = true;
        }
    }
}
