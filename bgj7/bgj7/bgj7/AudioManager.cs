﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace bgj7
{
    class AudioManager
    {
        AudioEngine engine;
        SoundBank soundBank;
        WaveBank waveBank;

        public AudioManager()
        {
            engine = new AudioEngine(@"Content/Thirst_Audio.xgs");
            soundBank = new SoundBank(engine, "Content\\Sound Bank.xsb");
            waveBank = new WaveBank(engine, "Content\\Wave Bank.xwb");
        }

        public void PlayCue(Cue cue)
        {
            cue.Play();
        }

        public Cue GetCue(string name)
        {
            try
            {
                return soundBank.GetCue(name);
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Couldn't find cue " + name + ".");
                return soundBank.GetCue("debug");
            }
        }

        internal void PlayCue(string name)
        {
            PlayCue(GetCue(name));
        }
    }
}
